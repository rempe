package com.javispedro.rempe;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static android.content.ContentValues.TAG;

class Preferences {
    public final static String PREFS_DEVICES = "devices";

    public static List<Integer> getDeviceNumbers(SharedPreferences prefs) {
        ArrayList<Integer> result = new ArrayList<Integer>();

        try {
            StringTokenizer st = new StringTokenizer(prefs.getString(PREFS_DEVICES, ""), ",");
            while (st.hasMoreTokens()) {
                result.add(Integer.parseInt(st.nextToken()));
            }
        } catch (java.lang.Exception ex) {
            // Ensure that at least we can recover from a corrupted preferences situation...
            Log.e(TAG, ex.toString());
        }

        return result;
    }

    public static void saveDeviceNumbers(SharedPreferences prefs, List<Integer> list) {
        StringBuilder sb = new StringBuilder();
        for (Integer i : list) {
            sb.append(i.toString());
            sb.append(",");
        }
        final String pref = sb.toString();
        Log.d(TAG, "saveDeviceNumbers: " + pref);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREFS_DEVICES, pref);
        editor.apply();
    }

    public static String getUnits(SharedPreferences prefs) {
        return prefs.getString("units", "celsius");
    }
}
