package com.javispedro.rempe;

import java.math.BigDecimal;

public class Reading {
    public long timestamp;

    public BigDecimal temperature;

    public BigDecimal lowLast24Hours;
    public BigDecimal highLast24Hours;
}
