package com.javispedro.rempe;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Device}.
 */
public class DeviceListRecyclerViewListAdapter extends RecyclerView.Adapter<DeviceViewHolder> {

    private List<Device> mList;

    public DeviceListRecyclerViewListAdapter() {
        setHasStableIds(true);
    }

    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        } else {
            return 0;
        }
    }

    @Override
    public long getItemId(int position) {
        return mList.get(position).getDeviceNumber();
    }

    @Override
    @NonNull
    public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_device, parent, false);
        return new DeviceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DeviceViewHolder holder, int position) {
        holder.setDevice(mList.get(position));
    }



    public void setDeviceList(List<Device> list) {
        mList = list;
    }
}