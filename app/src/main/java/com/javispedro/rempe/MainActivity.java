package com.javispedro.rempe;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListUpdateCallback;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";

    private SharedPreferences mPrefs = null;

    private final ArrayList<Integer> mDeviceNumbers = new ArrayList<Integer>();
    private final ArrayList<Device> mDevices = new ArrayList<Device>();

    private DeviceListRecyclerViewListAdapter mDeviceListAdapter;
    private SwipeRefreshLayout mSrlList;
    private RecyclerView mList;

    private Device mSearchDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fabAddDevice);
        fab.setOnClickListener(view -> onConnectButtonClicked());

        mSrlList = findViewById(R.id.srlList);
        mSrlList.setOnRefreshListener(() -> onListRefresh());

        mList = findViewById(R.id.list);
        mList.setLayoutManager(new LinearLayoutManager(mList.getContext()));
        mDeviceListAdapter = new DeviceListRecyclerViewListAdapter();
        mList.setAdapter(mDeviceListAdapter);

        new ItemTouchHelper(mItemTouchHelperCallback).attachToRecyclerView(mList);

        refreshDevices();
    }

    @Override
    protected void onDestroy() {
        disconnectAll();
        mPrefs = null;
        mDeviceListAdapter = null;
        mSrlList = null;
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPrefs.registerOnSharedPreferenceChangeListener(mPrefsListener);
        connectToDevices();
    }

    @Override
    public void onPause() {
        disconnectAll();
        mPrefs.unregisterOnSharedPreferenceChangeListener(mPrefsListener);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_open_antplus_plugin_manager:
                if (!AntPluginPcc.startPluginManagerActivity(this)) {
                    Log.e(TAG, "plugin manager not found");
                    showDependencyRequiredMsg("ANT+ plugin manager", "com.dsi.ant.plugins.antplus");
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onConnectButtonClicked() {
        searchForNewDevice();
    }

    private void onListRefresh() {
        connectToDevices();
        mSrlList.setRefreshing(false);
    }

    private final ItemTouchHelper.Callback mItemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            final int from = viewHolder.getAdapterPosition();
            final int to = viewHolder.getAdapterPosition();

            Log.d(TAG, "onMove from=" + from + " to=" + to);

            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            final int position = viewHolder.getAdapterPosition();
            removeDeviceByPosition(position);
        }
    };

    private final SharedPreferences.OnSharedPreferenceChangeListener mPrefsListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            switch (key) {
                case Preferences.PREFS_DEVICES:
                    refreshDevices();
                    break;
            }
        }
    };

    private final Device.DeviceObserver mSearchDeviceObserver = new Device.DeviceObserver() {
        @Override
        public void onDeviceSearchFinished(RequestAccessResult result) {
            Log.d(TAG, "onDeviceSearchFinished result=" + result);
            if (result == RequestAccessResult.SUCCESS) {
                final Device device = mSearchDevice;
                mSearchDevice.setObserver(null);
                mSearchDevice = null; // Ownership of mSearchDevice passed to addDevice below
                runOnUiThread(() -> addDevice(device));
            } else if (result == RequestAccessResult.DEPENDENCY_NOT_INSTALLED) {
                final String dependency = AntPluginPcc.getMissingDependencyName();
                final String packageName = AntPluginPcc.getMissingDependencyPackageName();
                runOnUiThread(() -> {
                    showDependencyRequiredMsg(dependency, packageName);
                });
            } else if (result != RequestAccessResult.USER_CANCELLED) {
                runOnUiThread(() -> {
                    final String resultText = Device.connectionRequestAccessResultToString(MainActivity.this, result);
                    Snackbar.make(findViewById(R.id.fabAddDevice),
                            getString(R.string.add_device_failed, resultText), Snackbar.LENGTH_INDEFINITE).show();
                });
            }
            if (mSearchDevice != null) {
                mSearchDevice.close();
                mSearchDevice = null;
            }
        }

        @Override
        public void onDeviceInfoChanged() {

        }

        @Override
        public void onDeviceStateChanged() {

        }

        @Override
        public void onDeviceNewReading() {

        }

        @Override
        public void onDeviceRssiChanged() {

        }
    };

    public void showDependencyRequiredMsg(String name, String packageName) {
        Log.d(TAG, "showDependencyRequiredMsg for " + name + " (" + packageName + ")");
        new MaterialAlertDialogBuilder(this)
                .setTitle(R.string.antplus_required_title)
                .setMessage(getString(R.string.antplus_required_msg, name))
                .setPositiveButton(R.string.antplus_required_go_to_store, (dialog, which) -> {
                    dialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=" + packageName));
                    startActivity(intent);
                })
                .setNeutralButton(R.string.antplus_required_close, (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }

    public void searchForNewDevice() {
        if (mSearchDevice != null) {
            mSearchDevice.close();
        }
        mSearchDevice = new Device();
        mSearchDevice.setObserver(mSearchDeviceObserver);
        mSearchDevice.searchForDevice(this);
    }

    public void addDevice(int deviceNumber) {
        Log.d(TAG, "addDevice deviceNumber=" + deviceNumber);
        if (deviceNumber == Device.INVALID_DEVICE) {
            Log.e(TAG, "trying to add invalid device number; ignoring");
            return;
        }
        List<Integer> list = Preferences.getDeviceNumbers(mPrefs);
        if (list.contains(deviceNumber)) {
            Log.w(TAG, "device already on list!");
            Snackbar.make(findViewById(R.id.fabAddDevice),
                    getString(R.string.add_device_already), Snackbar.LENGTH_INDEFINITE).show();
            return;
        }
        list.add(deviceNumber);
        Preferences.saveDeviceNumbers(mPrefs, list);
    }

    public void addDevice(Device device) {
        final int deviceNumber = device.getDeviceNumber();
        Log.d(TAG, "addDevice device.getDeviceNumber=" + deviceNumber);
        if (deviceNumber == Device.INVALID_DEVICE) {
            Log.e(TAG, "trying to add invalid device number; ignoring");
            return;
        }
        if (mDeviceNumbers.contains(deviceNumber)) {
            Log.w(TAG, "device already on list!");
            Snackbar.make(findViewById(R.id.fabAddDevice),
                    getString(R.string.add_device_already), Snackbar.LENGTH_INDEFINITE).show();
            device.close();
            return;
        }

        mDevices.add(device);
        mDeviceNumbers.add(deviceNumber);

        if (mDeviceListAdapter != null) {
            mDeviceListAdapter.notifyItemInserted(mDevices.size() - 1);
        }

        // Since we have manipulated mDeviceNumbers directly, even if we change the Prefs now
        // Prefs listener should end up doing nothing
        Preferences.saveDeviceNumbers(mPrefs, mDeviceNumbers);
    }

    public void removeDeviceByPosition(int position) {
        Log.d(TAG, "removeDeviceByPosition position=" + position);
        List<Integer> list = Preferences.getDeviceNumbers(mPrefs);
        try {
            list.remove(position);
        } catch (IndexOutOfBoundsException ex) {
            Log.w(TAG, ex);
        }

        Snackbar.make(mList,
                getString(R.string.remove_device_done), Snackbar.LENGTH_SHORT).show();

        // Prefs listener will take care of removing the device from mDevices
        Preferences.saveDeviceNumbers(mPrefs, list);
    }

    public void refreshDevices() {
        Log.d(TAG, "refreshDevices");

        setDeviceNumberList(Preferences.getDeviceNumbers(mPrefs));
    }

    public void connectToDevices() {
        Log.d(TAG, "connectToDevices");
        for (Device dev : mDevices) {
            if (!dev.isOpen()) {
                dev.connect(this);
            }
        }
    }

    public void disconnectAll() {
        Log.d(TAG, "disconnectAll");
        for (Device dev : mDevices) {
            dev.close();
        }
    }

    void setDeviceNumberList(List<Integer> newDeviceNumbers) {
            DiffUtil.DiffResult diff = DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return mDeviceNumbers.size();
            }

            @Override
            public int getNewListSize() {
                return newDeviceNumbers.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return mDeviceNumbers.get(oldItemPosition).equals(newDeviceNumbers.get(newItemPosition));
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return areItemsTheSame(oldItemPosition, newItemPosition);
            }
        });

        diff.dispatchUpdatesTo(new ListUpdateCallback() {
            @Override
            public void onInserted(int position, int count) {
                for (int i = 0; i < count; ++i) {
                    final int deviceNumber = newDeviceNumbers.get(position + i);
                    final String deviceName = getString(R.string.device_unnamed, deviceNumber);
                    Device device = new Device(deviceNumber, deviceName);
                    mDeviceNumbers.add(position + i, deviceNumber);
                    mDevices.add(position + i, device);
                }
            }

            @Override
            public void onRemoved(int position, int count) {
                for (int i = 0; i < count; ++i) {
                    mDevices.get(position + i).close();
                }
                mDevices.subList(position, position + count).clear();
                mDeviceNumbers.subList(position, position + count).clear();
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                mDevices.set(toPosition, mDevices.get(fromPosition));
                mDevices.set(fromPosition, null);
                mDeviceNumbers.set(toPosition, mDeviceNumbers.get(fromPosition));
                mDeviceNumbers.set(fromPosition, 0);
            }

            @Override
            public void onChanged(int position, int count, @Nullable Object payload) {
                // Nothing to be done
            }
        });

        if (mDeviceListAdapter != null) {
            mDeviceListAdapter.setDeviceList(mDevices);
            diff.dispatchUpdatesTo(mDeviceListAdapter);
        }
    }

    public List<Integer> getDeviceNumberList() {
        return mDeviceNumbers;
    }
}