package com.javispedro.rempe;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;

import java.math.BigDecimal;

public class DeviceViewHolder extends RecyclerView.ViewHolder implements Device.DeviceObserver {
    private static final String TAG = "DeviceViewHolder";

    private final View mView;
    private final Context mContext;

    private final SharedPreferences mPrefs;

    private final TextView mTemperatureView;
    private final TextView mMinMaxTemperatureLabel;
    private final TextView mMinTemperatureView;
    private final TextView mMaxTemperatureView;
    private final TextView mNameView;
    private final TextView mStatusView;
    private final ImageView mStatusImageView;
    private final ProgressBar mStatusProgressView;
    private final TextView mSignalLabel;
    private final ProgressBar mSignalBar;

    private final static BigDecimal C2F_MULT = new BigDecimal("1.8");
    private final static BigDecimal C2F_ADD = new BigDecimal("32");
    private final static BigDecimal C2K_ADD = new BigDecimal("273.15");

    private Device mDevice;

    public DeviceViewHolder(View view) {
        super(view);
        mView = view;
        mContext = view.getContext();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        mNameView = view.findViewById(R.id.nameView);
        mStatusView = view.findViewById(R.id.statusView);
        mStatusImageView = view.findViewById(R.id.statusImageView);
        mStatusProgressView = view.findViewById(R.id.statusProgressBar);
        mTemperatureView = view.findViewById(R.id.temperatureView);
        mMinMaxTemperatureLabel = view.findViewById(R.id.minmaxTemperatureLabel);
        mMinTemperatureView = view.findViewById(R.id.minTemperatureView);
        mMaxTemperatureView = view.findViewById(R.id.maxTemperatureView);
        mSignalBar = view.findViewById(R.id.signalBar);
        mSignalLabel = view.findViewById(R.id.signalLabel);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + mNameView.getText() + "'";
    }

    public void setDevice(Device device) {
        resetDisplay();
        if (mDevice != null) {
            mDevice.setObserver(null);
        }
        mDevice = device;
        if (mDevice != null) {
            mDevice.setObserver(this);
            onDeviceInfoChanged();
            onDeviceStateChanged();
        }
    }

    private void runOnUiThread(Runnable r) {
        mView.post(r);
    }

    private void resetDisplay() {
        mTemperatureView.setText(mContext.getString(R.string.temperature_nothing));
        mMinMaxTemperatureLabel.setVisibility(View.INVISIBLE);
        mMinTemperatureView.setText(mContext.getString(R.string.temperature_nothing));
        mMaxTemperatureView.setText(mContext.getString(R.string.temperature_nothing));
        mNameView.setText("");
        mStatusView.setText("");
        mStatusImageView.setVisibility(View.INVISIBLE);
        mStatusProgressView.setVisibility(View.INVISIBLE);
        mSignalLabel.setVisibility(View.INVISIBLE);
        mSignalBar.setProgress(0);
    }

    private String formatTemperature(BigDecimal tempC) {
        switch (Preferences.getUnits(mPrefs)) {
            default:
            case "celsius":
                return mContext.getString(R.string.temperature_celsius, tempC);
            case "fahrenheit":
                BigDecimal tempF = (tempC.multiply(C2F_MULT)).add(C2F_ADD);
                return mContext.getString(R.string.temperature_fahrenheit, tempF);
            case "kelvin":
                BigDecimal tempK = (tempC.add(C2K_ADD));
                return mContext.getString(R.string.temperature_kelvin, tempK);
        }
    }

    @Override
    public void onDeviceSearchFinished(RequestAccessResult result) {
        // Nothing to do here
    }

    @Override
    public void onDeviceInfoChanged() {
        runOnUiThread(() -> {
            mNameView.setText(mDevice.getDeviceName());
        });
    }

    @Override
    public void onDeviceStateChanged() {
        runOnUiThread(() -> {
            if (mDevice.getConnectResult() != RequestAccessResult.SUCCESS) {
                mStatusView.setText(Device.connectionRequestAccessResultToString(mContext, mDevice.getConnectResult()));
                mStatusImageView.setImageResource(R.drawable.ic_baseline_error_24);
                mStatusProgressView.setVisibility(View.INVISIBLE);
                mStatusImageView.setVisibility(View.VISIBLE);
            } else {
                mStatusView.setText(Device.deviceStateToString(mContext, mDevice.getCurrentDeviceState()));
                switch (mDevice.getCurrentDeviceState()) {
                    case TRACKING:
                        mStatusImageView.setImageResource(R.drawable.ic_baseline_check_24);
                        mStatusProgressView.setVisibility(View.INVISIBLE);
                        mStatusImageView.setVisibility(View.VISIBLE);
                        break;
                    case SEARCHING:
                    case PROCESSING_REQUEST:
                        mStatusImageView.setVisibility(View.INVISIBLE);
                        mStatusProgressView.setVisibility(View.VISIBLE);
                        break;
                    case DEAD:
                        mStatusImageView.setImageResource(R.drawable.ic_baseline_error_24);
                        mStatusProgressView.setVisibility(View.INVISIBLE);
                        mStatusImageView.setVisibility(View.VISIBLE);
                        break;
                    default:
                        mStatusImageView.setVisibility(View.INVISIBLE);
                        mStatusProgressView.setVisibility(View.INVISIBLE);
                        break;
                }
            }
        });
    }

    @Override
    public void onDeviceNewReading() {
        runOnUiThread(() -> {
            final Reading reading = mDevice.getLastReading();
            mTemperatureView.setText(formatTemperature(reading.temperature));
            mMinMaxTemperatureLabel.setVisibility(View.VISIBLE);
            mMinTemperatureView.setText(formatTemperature(reading.lowLast24Hours));
            mMaxTemperatureView.setText(formatTemperature(reading.highLast24Hours));
        });
    }

    private int rssiToMeterValue(float rssi) {
        final float minThreshold = -100;
        final float maxThreshold = 0;

        final float step = (maxThreshold - minThreshold) / 100;

        return Math.round((rssi - minThreshold) / step);
    }

    @Override
    public void onDeviceRssiChanged() {
        runOnUiThread(() -> {
            mSignalBar.setProgress(rssiToMeterValue(mDevice.getLastRssi()));
            mSignalLabel.setVisibility(View.VISIBLE);
        });
    }
}
