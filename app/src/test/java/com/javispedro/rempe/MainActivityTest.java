package com.javispedro.rempe;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class MainActivityTest {
    private final static String TAG = "MainActivityTest";

    @Test
    public void setDeviceNumberList() {
        final MainActivity activity = new MainActivity();
        final Random r = new Random();

        ArrayList<Integer> list = new ArrayList<Integer>();
        activity.setDeviceNumberList(list);
        assertEquals(list, activity.getDeviceNumberList());

        list.add(r.nextInt(30000));
        activity.setDeviceNumberList(list);
        assertEquals(list, activity.getDeviceNumberList());

        for (int i = 0; i < 400; ++i) {
            list.add(r.nextInt(list.size()), r.nextInt(60000));
            activity.setDeviceNumberList(list);
            assertEquals(list, activity.getDeviceNumberList());

            list.add(r.nextInt(list.size()), r.nextInt(60000));
            activity.setDeviceNumberList(list);
            assertEquals(list, activity.getDeviceNumberList());

            list.remove(r.nextInt(list.size() - 1));
            activity.setDeviceNumberList(list);
            assertEquals(list, activity.getDeviceNumberList());
        }
    }
}